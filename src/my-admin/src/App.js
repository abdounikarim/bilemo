import React from 'react';
import { HydraAdmin } from '@api-platform/admin';

export default () => <HydraAdmin entrypoint="https://admin.localhost"/>; // Replace with your own API entrypoint